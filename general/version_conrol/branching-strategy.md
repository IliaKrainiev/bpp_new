# Branching Strategies

<br/>

<br/>

- [Feature branching](./strategies/feature-branching.md)
- [GitFlow](./strategies/gitflow.md)
- [GitOps](./strategies/gitops.md)
- [Trunk-Based Development](./strategies/trunkbased.md) (Work In Progress️ ✍️)
- [Release Branching](./strategies/release.md)
