# Gitignore File: How to Ignore Files and Folders in Git

In Git, the `.gitignore` file allows you to specify files and folders that should be ignored and not tracked by Git. This is useful for excluding files and directories that are not relevant to your project or should not be committed.

### What to include to `.gitignore`

```
# Dependency directories
/node_modules
/bower_components

# Build output
/build
/dist
/out

# Environment variables
.env.local
.env.development.local
.env.test.local
.env.production.local

# IDE and editor-specific files
.idea
.vscode
*.sublime-project
*.sublime-workspace

# Compiled code and binaries
*.log
*.sqlite

# OS-generated files
.DS_Store
Thumbs.db

# Custom
# Add your custom rules here

```

### Observe `.gitignore` file patterns. Each pattern should be on a new line.

Here are some common patterns you can use:

- Ignore a specific file:

  ```
  filename.txt
  ```

- Ignore all files with a specific extension:

  ```
  *.log
  ```

- Ignore all files in a specific directory:

  ```
  folder/
  ```

- Ignore all files and directories matching a specific pattern:

  ```
  logs/*.log
  ```

- Exclude a directory but include a specific file within it:
  ```
  folder/
  !folder/specific-file.txt
  ```

### How to Ignore a Previously Committed File

Make sure the file is listed in your .gitignore file. If it's not already included, add the file or pattern to exclude it from future commits.

Remove the file from Git's tracking without deleting it from your local file system. Run the following command:

```
git rm --cached <file>
```

Replace <file> with the name or path of the file you want to ignore.

Commit the changes to reflect the removal of the file from Git's tracking:

```
git commit -m "Stop tracking <file>"
```

Replace <file> with the name of the file you want to ignore.

The file is now ignored in future commits. However, please note that this process only affects future commits and does not remove the file from Git's history. The file may still be present in previous commits.

If you want to completely remove the file from your repository's history, it requires rewriting the Git history. However, modifying Git history can have unintended consequences and is generally discouraged, especially if your repository is shared with others. Exercise caution and consult the Git documentation on rewriting history for more advanced techniques if necessary.

Remember to communicate any changes to your collaborators to ensure everyone is aware of the modifications and to avoid conflicts.

Please be aware that if the file you want to ignore contains sensitive information (e.g., passwords, API keys), you should also take steps to ensure the security of that information, such as revoking and replacing any compromised credentials
