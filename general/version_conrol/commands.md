To remove a file from the Git history, you can use the git filter-branch command. This command allows you to rewrite the entire history of your repository, including removing specific files. However, please exercise caution when modifying Git history, especially if your repository is shared with others. Here's a general outline of the steps involved

git filter-branch --index-filter 'git rm --cached --ignore-unmatch <file>' --prune-empty -- --all

The git log command is used to display the commit history in a Git repository. It provides valuable information about the commits, such as the commit hash, author, date, commit message, and the changes introduced in each commit. Here are some examples of how you can use git log:

Explore last commit

```
git log -n 1
```

Search by author

```
git log --author="John Doe"

```

View the commit history for a specific branch

```
git log main

```
