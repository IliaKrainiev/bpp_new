#!/bin/bash 

version=$1;
locale=$2;
current_branch=$(git branch --show-current)

if [ -z "$locale" ]
    then cp -R locales ../buffer
else 
    cp -R locales/$locale ../buffer
fi

git checkout release/v$version

if [ -z "$locale" ]
    then 
        for folder in ../buffer/*; do
            for file in $folder/*; do
                filename=$(basename $file)
                foldername=$(basename $folder)
                mkdir -p ../diff/$foldername
                echo $folder/$filename
                if test -f locales/$foldername/$filename; then
                    json-diff --output-new-only -j locales/$foldername/$filename ../buffer/$foldername/$filename  >  ../diff/$foldername/$filename
                else  echo "locales/$foldername/$filename - does not exists in $version version"
                fi
            done
    done
else 
     for file in ../buffer/*; do
        mkdir -p ../diff
        filename=$(basename $file)
        if test -f locales/$locale/$filename; then
            json-diff --output-new-only -j ../buffer/$filename locales/$locale/$filename  >  ../diff/$filename
        else echo "locales/$locale/$filename - does not exists in $version version"
        fi
    done
fi

git checkout $current_branch

if [ -z "$locale" ]; then   
    cp -R ../diff ./diff_with_$version 
else  
    mkdir -p ./diff_with_$version
    cp -R ../diff ./diff_with_$version/$locale
fi


rm -r ../buffer
rm -r ../diff
