const { removeRedundantTranslations } = require('./common')

removeRedundantTranslations([
  'fr',
  'fr-FR',
  'nl',
  'nl-NL',
  'es-MX',
  'zh',
  'zh-CN',
  'de',
  'de-DE',
  'es-CO',
])
