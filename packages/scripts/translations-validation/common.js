const { readdirSync, statSync, writeFileSync } = require('fs')
const flatten = require('flat')
const set = require('lodash.set')
const uniq = require('lodash.uniq')
const difference = require('lodash.difference')
const pick = require('lodash.pick')

const passedLocales = process.argv.slice(2)
const FILE_NAME_UI = 'ui'
const FILE_NAME_SERVER = 'server'
const PATH = `${__dirname}/../locales`
const DEFAULT_LOCALE = 'en'

const FILES = ['countries', FILE_NAME_SERVER, FILE_NAME_UI]

const getDirectories = (path) =>
  readdirSync(path).filter((file) => statSync(`${path}/${file}`).isDirectory())
const directoriesList = getDirectories(PATH)

const removeRedundantTranslations = (neededLocales) => {
  const defaultKeys = getKeysByLocale(DEFAULT_LOCALE)
  localesForCheck.forEach((locale) => {
    if (locale === DEFAULT_LOCALE) {
      return
    }
    if (neededLocales.includes(locale)) {
      console.log(locale)

      ;[FILE_NAME_SERVER, FILE_NAME_UI].forEach((fileName) => {
        const langObj = flatten(require(`${PATH}/${locale}/${fileName}.json`))
        writeFileSync(
          `${PATH}/${locale}/${fileName}.json`,
          JSON.stringify(pick(langObj, defaultKeys[fileName]), null, 4)
        )
      })
    }
  })
}

const localesForCheck =
  passedLocales.length > 0 ? passedLocales : directoriesList
const getKeysByLocale = (locale) => {
  const keysByLocale = {}

  FILES.forEach((fileName) => {
    keysByLocale[fileName] = Object.keys(
      flatten(require(`${PATH}/${locale}/${fileName}.json`))
    )
  })

  return keysByLocale
}

const getValuesByLocale = (locale) => {
  const valuesByLocale = {}

  FILES.forEach((fileName) => {
    valuesByLocale[fileName] = flatten(
      require(`${PATH}/${locale}/${fileName}.json`)
    )
  })

  return valuesByLocale
}
const getListOfParams = (text) => {
  const params = []
  const pattern = /{([^}]+)}/g
  let currentMatch

  while ((currentMatch = pattern.exec(text))) {
    params.push(currentMatch[1])
  }

  return params
}

const getMissedTranslaltions = (localesForCheck) => {
  const defaultKeys = getKeysByLocale(DEFAULT_LOCALE)
  const missedTranslations = {}

  localesForCheck.forEach((locale) => {
    if (locale !== DEFAULT_LOCALE) {
      const localeKeys = getKeysByLocale(locale)

      FILES.forEach((fileName) => {
        const diffKeys = difference(defaultKeys[fileName], localeKeys[fileName])

        if (diffKeys.length > 0) {
          set(missedTranslations, `${locale}.${fileName}`, diffKeys)
        }
      })
    }
  })

  return missedTranslations
}

const getIncorrectlyNamedParams = (localesForCheck) => {
  const defaultValues = getValuesByLocale(DEFAULT_LOCALE)
  const wrongTranslations = {}

  localesForCheck.forEach((locale) => {
    if (locale !== DEFAULT_LOCALE) {
      const localeValues = getValuesByLocale(locale)

      FILES.forEach((fileName) => {
        const defaultValuesByFileName = defaultValues[fileName]
        const localeValuesByFileName = localeValues[fileName]

        for (let key in defaultValuesByFileName) {
          const diffParams = difference(
            getListOfParams(defaultValuesByFileName[key]),
            getListOfParams(localeValuesByFileName[key])
          )

          if (diffParams.length > 0 && localeValuesByFileName[key]) {
            wrongTranslations[`${locale}.${fileName}.${key}`] = diffParams
          }
        }
      })
    }
  })

  return wrongTranslations
}

const ON_KEY = 'SwitchField.on'
const OFF_KEY = 'SwitchField.off'
const getIncorrectOnOffValues = (localesForCheck) => {
  const wrongTranslations = []

  localesForCheck.forEach((locale) => {
    if (locale !== DEFAULT_LOCALE) {
      const localeValues = getValuesByLocale(locale)

      if (
        localeValues[FILE_NAME_UI][ON_KEY] !== ' ' ||
        localeValues[FILE_NAME_UI][OFF_KEY] !== ' '
      ) {
        wrongTranslations.push(locale)
      }
    }
  })

  return wrongTranslations
}

const TIME_PICKER_AM = 'Components.TimePicker.AM'
const TIME_PICKER_PM = 'Components.TimePicker.PM'
const EXAM_METHOD_PLACEHOLDER = 'Exam.methodPlaceholder'
const getIncorrectAmPmValues = (localesForCheck) => {
  const wrongTranslations = []

  localesForCheck.forEach((locale) => {
    if (locale !== DEFAULT_LOCALE) {
      const localeValues = getValuesByLocale(locale)

      if (
        localeValues[FILE_NAME_UI][TIME_PICKER_AM] !== 'AM' ||
        localeValues[FILE_NAME_UI][TIME_PICKER_PM] !== 'PM' ||
        localeValues[FILE_NAME_UI][EXAM_METHOD_PLACEHOLDER] !== 'AM / PM'
      ) {
        wrongTranslations.push(locale)
      }
    }
  })

  return wrongTranslations
}

const KEYS_WITHOUT_DOTS_UI = [
  'VideoConference.Tooltip.shareScreenActive',
  'VideoConference.Tooltip.shareScreenInactive',
]
const KEYS_WITHOUT_DOTS_SERVER = [
  'Notifications.Sms.appointmentCancelled',
  'Notifications.Sms.appointmentChanged',
  'Notifications.Sms.appointmentChangedNearFuture',
  'Notifications.Sms.appointmentNew',
  'Notifications.Sms.appointmentReminder',
]
const getIncorrectEndByDotsValues = (localesForCheck) => {
  const wrongTranslations = []

  localesForCheck.forEach((locale) => {
    if (locale !== DEFAULT_LOCALE) {
      const localeValues = getValuesByLocale(locale)

      KEYS_WITHOUT_DOTS_UI.forEach((key) => {
        const loacaleServer = localeValues[FILE_NAME_UI]
        const isDotAtEnd =
          loacaleServer[key].endsWith('.') || loacaleServer[key].endsWith('。')
        const isDotAtStart =
          loacaleServer[key].startsWith('.') ||
          loacaleServer[key].startsWith('。')

        if (isDotAtEnd || isDotAtStart) {
          wrongTranslations.push(locale)
        }
      })

      KEYS_WITHOUT_DOTS_SERVER.forEach((key) => {
        const loacaleServer = localeValues[FILE_NAME_SERVER]
        const isDotAtEnd =
          loacaleServer[key].endsWith('.') || loacaleServer[key].endsWith('。')
        const isDotAtStart =
          loacaleServer[key].startsWith('.') ||
          loacaleServer[key].startsWith('。')

        if (isDotAtEnd || isDotAtStart) {
          wrongTranslations.push(locale)
        }
      })
    }
  })

  return uniq(wrongTranslations)
}

const resultLogger = (text, result) => {
  console.log(text)
  console.log(result)
  console.log(
    `--------------------------------------------------------------------------\n`
  )
}

module.exports = {
  removeRedundantTranslations,
  resultLogger,
  ON_KEY,
  OFF_KEY,
  TIME_PICKER_AM,
  TIME_PICKER_PM,
  KEYS_WITHOUT_DOTS_UI,
  KEYS_WITHOUT_DOTS_SERVER,
  localesForCheck,
  getIncorrectlyNamedParams,
  getMissedTranslaltions,
  getIncorrectOnOffValues,
  getIncorrectEndByDotsValues,
  getIncorrectAmPmValues,
}
