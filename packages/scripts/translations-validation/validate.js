const {
  getIncorrectAmPmValues,
  resultLogger,
  ON_KEY,
  OFF_KEY,
  TIME_PICKER_AM,
  TIME_PICKER_PM,
  KEYS_WITHOUT_DOTS_UI,
  KEYS_WITHOUT_DOTS_SERVER,
  localesForCheck,
  getIncorrectlyNamedParams,
  getMissedTranslaltions,
  getIncorrectOnOffValues,
  getIncorrectEndByDotsValues,
} = require('./common')

resultLogger('Missed translations:', getMissedTranslaltions(localesForCheck))
resultLogger(
  'Incorrectly named params in values:',
  getIncorrectlyNamedParams(localesForCheck)
)
resultLogger(
  `Incorrect ON / OFF values in locales for the keys
    ${ON_KEY}, ${OFF_KEY}:`,
  getIncorrectOnOffValues(localesForCheck)
)
resultLogger(
  `Incorrect AM / PM  values in locales for the keys
    ${TIME_PICKER_AM}, ${TIME_PICKER_PM}:`,
  getIncorrectAmPmValues(localesForCheck)
)
resultLogger(
  `Incorrect dots at the end for values of some of the keys
    ${KEYS_WITHOUT_DOTS_UI.concat(KEYS_WITHOUT_DOTS_SERVER).join(', ')}:`,
  getIncorrectEndByDotsValues(localesForCheck)
)
