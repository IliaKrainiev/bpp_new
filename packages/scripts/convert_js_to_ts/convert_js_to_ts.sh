#!/bin/bash

# Find all JavaScript files recursively
find /path/to/directory -type f -name "*.js" |
while read -r js_file; do
  # Replace the .js extension with .ts
  ts_file="${js_file%.js}.ts"
  
  # Rename the JavaScript file to TypeScript file
  mv "$js_file" "$ts_file"
done
