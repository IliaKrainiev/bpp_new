## How to create my own package ?

Please, consider usage of [vite](https://vitejs.dev/guide/).

Use [node version 18](https://nodejs.org/uk)

```
    npm create vite@latest my-vue-app -- --template vue
```

Consider for adding styling to yours example if needed.

- [Tailwind + Vite](https://tailwindcss.com/docs/guides/vite)
