# <img src="/assets/star.png" width="36" height="36" /> Front End Best Practices

<br/>

<br/>
<br/>
<br/>
<h2>Table of Contents </h2>

<details>
  <summary>
    <a href="#general">General</a>
  </summary>

<div>&emsp;&emsp; [Version control](./general/version_conrol/index.md)</div>
<div> &emsp;&emsp;[Cross Browser Compatibility](./general/cross_browser_compatibility/index.md)</div>
<div> &emsp;&emsp;[Code syntax & Formatting](./general/code_syntax_and_formatting/index.md)</div>

</details>

<details>
  <summary>
    <a href="#2-error-handling-practices">React </a>
  </summary>

<div>&emsp;&emsp;[Code Organization and Modularity in React](./react/code_organization_and_modularity/index.md)</br></div>
<div> &emsp;&emsp;[Accessibility](./react/accessibility/index.md)</div>
<div> &emsp;&emsp;[Perfomance Optimization](./react/perfomance-optimization/index.md)</div>
<div> &emsp;&emsp;[Testing](./react/testing/index.md)</div>
<div> &emsp;&emsp;[Localization](./react/localization/index.md)</div>

</details>

<br/><br/>
