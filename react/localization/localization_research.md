# Localization approaches research

<table>
    <thead>
        <tr>
            <th></th>
            <th>https://locize.com/</th>
            <th>https://phrase.com/</th>
            <th>https://gemino.de/en/</th>
            <th>https://github.com/formatjs/react-intl</th>
            <th>https://lokalise.com</th>
            <th>https://localizejs.com/</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>PROS</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <strong>Web-editor tool</strong>
                </br> Adding one line of code to your website is all needed to get started. All your content gets extracted and sent to your locize project ready to be translated.</br></br> Every change to your translations gets instantly reflected on your website and vice versa. </br></br> Never again copy/paste translations from a to b.
                <ul><li>In-context editor enables you to directly update your translations from within your website.</li><li> Keep track over all changes done to your project's translations.The last translation changes are audited per key.</li> </ul>
            </td>
            <td>
                <strong>Web-editor tool</strong></br></br><strong>Translation Center</strong></br></br>The Translation Center is the backbone of the Phrase software localization platform. The web interface lets you manage all languages and translations across various platforms.</br></br><strong>In-Context Editor</strong></br></br>The Phrase In-Context Editor can be integrated within almost any web application and provides the possibility to translate content directly on your site.
            </td>
            <td>
            <strong>WebReview</strong></br></br>WebReview allows translations to be checked in context in the browser. Changes, comments or additions can be passed on directly to the translation teams as constructive feedback.</br></br><strong>TermCloud</strong></br></br>With TermCloud, any employee can look up specialist terms and required corporate terminology directly in their browser – hassle-free, and in all the relevant languages to your business. No training. No installation. No maintenance.
            </td>
            <td>
            <strong>Pricing.</strong></br></br>Free
            </td>
            <td>
                <strong>Web-editor tool</strong></br></br>You can load a screen from your app and lokalise automatically will recognize all messages that you already specified and after that you can try to translate to another language and check if it looks good or not.
            </td>
            <td>
                <strong>Web-editor tool</strong></br></br>With Localize you can easily review translation revision history, including translation source and date, making it easy to revert back to previous versions if desired. The Localize In-Context Editor lets translators see exactly where each phrase appears on your website and how their translation will look on the page.
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <strong>Smart translations</strong></br></br>The smart translation memory provides you with suggestions of already translated segments and helps you to save time and keeping your translations consistent.
            </td>
            <td>
                <strong>Provides a lot of features for the whole team</strong></br></br><ul><li>designers</li><li>developers</li><li>translators</li><li>managers</li></ul></br>For example: dashboard, review workflow, proofreading, etc.
            </td>
            <td>Not clear from info on the website.</td>
            <td>
              <strong>Already integrated</strong></br></br>Currently we are using this solution for translations and we need to do some additional small changes in our app to improve it and to support multi-language mode.
            </td>
             <td>
              <strong>Dashboard</strong></br></br>You can easily manage all your translations using really powerful filters (get only reviewed translation for example).
You can see history of translation changes and write comments (like Zeplin)
            </td>
            <td>
                <strong>Dashboard</strong></br></br>Invite your translators directly to your Localize project. Localize provides a full-featured TMS that allows your translators to translate your content directly within their Localize dashboard.</br></br><i> No more emailing spreadsheets and files back and forth!</i></br></br>Machine translations from Google and Microsoft are included at no charge with every plan, and provide an excellent way to get started quickly on your localization project.
            </td>
        </tr>
        <tr>
            <td></td>
           <td>
                <strong>Translation agencies</strong></br></br>Has agencies on the platform, or can invite your own agency. Usually it takes to several business days to get the translations</br></br>https://docs.locize.com/guides-tips-and-tricks/working-with-translators</br></br>Pricing for translation services </br></br>https://locize.com/services.html#translationservices
            </td>
            <td>
                <strong>Translation agencies</strong></br></br>Сan either work with your own team of translators or simply order professional translations from our translation partners. Rolling out new languages is now possible within hours. </br></br>https://phrase.com/lp/translation-order/
            </td>
             <td>Not clear from info on the website.</td>
             <td></td>
            <td>
                <strong>Agile localization approach</strong></br></br>https://blog.lokalise.com/agile-localization/
            </td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <strong>Review workflow</strong></br></br>You can enable the review workflow for specific languages.</br></br>This way each time someone changes a translation, it will start a review workflow. The actual value will not be changed until someone will accept one of the translation proposals.
            </td>
            <td>
                Provides a demo page, where we can check the flow:</br>https://phrase.com/demo/
            </td>
            <td>Not clear from info on the website.</td>
            <td></td>
            <td>
            <strong>QA checker</strong></br></br>Also there is built-in grammar checker, which can facilitate QA work.
            </td>
            <td>
                        <strong>Proofreading</strong></br></br>Need a second pair of eyes? Our in-progress workflow gives you the option to review translations and request changes if needed prior to publishing.
            </td>
        </tr>
        <tr>
        <td></td>
        <td>
            <strong>Experience at Star</strong></br></br>This tool is being used on other projects at Star with good feedbacks (no complains, up to several days translations are ready after EN texts are submitted)
        </td>
        <td>
                    <strong>Integrations</strong></br></br>Provides a lot of different integrations that can be useful for all team members.</br>https://phrase.com/integrations/
        </td>
        <td>
            Not clear from info on the website.
        </td>
        <td></td>
        <td>
        <strong>Integrations with JIRA, GitLab.</strong></br></br>You can create tasks for translation team, and review the result.</br></br>You can create pull requests from Lokalise to our GitLab Repo.
        </td>
        <td>
        <strong>Integrations</strong>https://localizejs.com/integrations
        </td>
        </tr>
         <tr>
        <td></td>
        <td>
        <strong>Privacy</strong></br></br>https://locize.com/privacy.html
        </td>
        <td>
        <strong>Privacy</strong></br></br>https://phrase.com/privacy/
        </td>
        <td>Not clear from info on the website.</td>
        <td></td>
        <td>
        <strong>Security</strong></br><i>GDPR compliance</i></br></br>https://lokalise.com/product-security
        </td>
        <td>
                <strong>Security</strong></br><i>GDPR compliance</i></br></br>https://localizejs.com/features/security-compliance
        </td>
        </tr>
                <tr>
        <td></td>
        <td>
        <strong>Case studies</strong></br>Also Star has a positive experience working with this platform.</br>https://locize.com/customers.html
        </td>
        <td>
        <strong>Case studies</strong><ul><li>HIRED</li><li>Fair Trade</li><li>Pulse</li><li>RIDECELL</li><li>BOSH</li></ul>
        </td>
        <td>
                <strong>Case studies</strong><ul><li>ELOPAR</li><li>ZEISS</li><li>CROHE</li><li>LNS</li></ul>
        </td>
        <td></td>
        <td>
                        <strong>Case studies</strong><ul><li>PADDYPOWER</li><li>SPECIALIZED</li><li>JODEL</li><li>BMW</li><li>shopify</li><li>Massimo Dutti</li><li>honestbee</li><li>STATS</li><li>IDT</li><li>XSOIIA</li><li>BAYER</li><li>Lunar way</li></ul>
        </td>
        <td>
                <strong>Case studies</strong><ul><li>Rocketmiles</li><li>Trello</li><li>Microsofy</li><li>DUO</li><li>intuit</li><li>Cisco</li><li>Meraki</li><li>StatusPage.io</li><li>Bulova</li><li>tinder</li></ul>
</td>
        </tr>
                <tr>
        <td></td>
        <td></td>
        <td>
        <strong>Maintenance & Support Efforts</strong></br></br>Translation team can work independently from engineering team, last minute changes are possible with minimized impact on release timeline;</br></br>Translation team can work on translations in parallel with new features not to leave it to the very end before release
        </td>
        <td>
        Not clear from info on the website.
        </td>
        <td></td>
        <td>
        <strong>Maintenance & Support Efforts</strong></br></br>Translation team can work independently from engineering team, last minute changes are possible with minimized impact on release timeline;</br></br>Translation team can work on translations in parallel with new features not to leave it to the very end before release
        </td>
        <td><strong>Maintenance & Support Efforts</strong></br></br>Translation team can work independently from engineering team, last minute changes are possible with minimized impact on release timeline;</br></br>Translation team can work on translations in parallel with new features not to leave it to the very end before release</td>
        </tr>
        <tr>
            <td>CONS</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
            <strong>Price</strong></br>Starting from $5 monthly (unlimited users, languages, files), but the cost is built up based on # of words, edits, downloads</br>https://locize.com/pricing.html</br>Additional costs if working with their translation partners: https://locize.com/services.html#translationservices</br>Price calculation example: https://docs.locize.com/more/general-questions/why-is-the-pricing-so-complicated</br>We must take into account the number of already existing locales and the number of keys
            </td>
            <td>
                <strong>Price</strong></br>Pricing is based on the users number that uses this tool.</br>In case if we want to have "In-Context" Editor it means that we should have at least "Advanced" plan, where the price is 35$ per user, with the minimum number of 5 users.</br>https://phrase.com/pricing/
            </td>
            <td>
            <strong>Price</strong></br>Could not find any information
            </td>
            <td>
                        <strong>Scalability</strong></br>By increasing the number of translations, the time for verification will be markedly increased
            </td>
            <td>
            <strong>Price</strong></br>https://lokalise.com/pricing
            </td>
            <td>
            <strong>Price</strong></br>Much more expensive solution even in comparison even to 'localise' and is per language and not per number of keys</br>https://localizejs.com/pricing</br>
            </td>
        </tr>
    </tbody>

</table>
