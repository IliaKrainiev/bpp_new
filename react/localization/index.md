# Localization in React applications

Here are some best practices and recommendations for effectively using react-i18n in your React application:

- **Define your loclaization strategy.**
  Build the flow of localization management or consider usage of [Translation Management Tools](#translation-management-tools).

- **Continuous Localization**
  Implement a process for continuous localization to easily add new translations or update existing ones as your application evolves. Automate the extraction and merging of translations from the source code to the localization files.

- **Organize Localization Files**
  Structure your localization files based on key themes or components rather than having a single monolithic file. This helps with maintainability and reduces potential conflicts when multiple translators are working on different sections.

- **Use Descriptive Keys**
  Choose descriptive keys for the translated strings. Avoid using arbitrary or cryptic keys that can be hard to understand or maintain. Clear and meaningful keys make it easier to manage and update translations.This helps translators understand the usage and context of each string, improving the accuracy and quality of the translations.

- **Use Language Codes**
  Use standard language codes (e.g., en for English, fr for French) for identifying languages in your application. This improves consistency and interoperability with other systems or libraries.

- **Test Translations**
  Test your localized application thoroughly across different languages, character sets, and right-to-left (RTL) languages. Pay attention to edge cases, such as string length, date formats, and numeric values, to ensure the translations fit properly within UI components and don't introduce layout or functional issues.
  Check external application that integrated into your application (pdf-renderer).

- **Consider Pluralization and Gender**
  Take into account pluralization and gender variations in different languages when designing your localization system. Provide clear guidelines to translators for handling these cases to ensure accurate and consistent translations.

- **Review translations changes**
  Translations is not easy to test. To simplify work of our QA collegues please review each change of localization carefully. It will help you to reduce time on re-testing and fixing related issues.

- **Store localization files on the Backend**
  This will make possible to simple reuse of translations for example for mobile applications. As well, Backend application probably will also need to use translations in order to send user emails etc.

- **Localization as a Service**
  Try to design localization flow to make it less coupled to all application. Consider storing translation files in Cloud Storages or using a [3rd parties](#translation-management-tools).
- **Optimize localization fetching**
  Setup your flow of translations fetching in order to reduce an amount of possible requests. Consider different caching options (cache on the Backend or FrontEnd)
- **Validate translations**
  If you choosed manual way of translations management consider adding a script to validate and omit all possible translations errors.
  Missed dots, wrong parameter name, incorrect pluralization and etc. [Please see the example](../../packages/scripts/translations-validation)

## Most Popular Solutions to build Localization for React applications

- [Format.JS](https://formatjs.io/docs/react-intl/)

- [i18next](https://react.i18next.com/)
  [Example](../../packages/react-localization/)

## Translation Management Tools

- [Locize](https://locize.com/)
- [Lokalise](https://lokalise.com/)

## Resouces:

[Please, check the research conducted for one of Star projects](./localization_research.md)
[Use the validation script](../../packages/scripts/translations-validation/)
